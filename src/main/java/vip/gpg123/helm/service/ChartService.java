package vip.gpg123.helm.service;

import org.springframework.web.multipart.MultipartFile;
import vip.gpg123.helm.util.R;

import java.io.IOException;

/**
 * @author gaopuguang
 */
public interface ChartService {

    /**
     * 上传chart文件
     *
     * @param file chart文件
     */
    R<Boolean> uploadChart(MultipartFile file) throws IOException;

    /**
     * 根据id查询
     *
     * @param id id
     */
    R<Object> searchChartById(String id) throws IOException;

    /**
     * 根据名称查询
     * @param name 名称
     */
    R<Object> searchChartByName(String name,String version) throws IOException;

    /**
     * 根据名称获取id
     * @param name 名称
     */
    R<String> getChartIdByName(String name,String version) throws IOException;

    /**
     * 查询全部
     */
    R<Object> searchAllChart() throws IOException;

    /**
     * 根据id删除
     * @param id id
     */
    R<Boolean> deleteChartById(String id) throws IOException;
}
