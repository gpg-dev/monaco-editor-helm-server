package vip.gpg123.helm.web;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.http.HttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yaml.snakeyaml.Yaml;
import vip.gpg123.helm.domain.HelmResultVo;
import vip.gpg123.helm.service.HelmOperateService;
import vip.gpg123.helm.util.R;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

/**
 * @author gaopuguang
 * @date 2023/7/28 11:04
 **/
@RestController
@RequestMapping("/yaml")
public class YamlController {

    @Autowired
    private HelmOperateService helmOperateService;


    /**
     * 文件获取
     */
    @GetMapping("/json")
    public R<Object> yaml2Json() {
        Yaml yaml = new Yaml();
        String filePath = "D:\\Project\\server\\helm-repo\\app\\values.yaml";
        Map map = yaml.loadAs(FileUtil.getInputStream(filePath), Map.class);
        Object obj = map.get("image");
        System.out.println(obj);
        return R.ok(map);
    }

    /**
     * gitee获取
     */
    @GetMapping("/gitee")
    public R<Object> byGitee(@RequestParam("name") String name, @RequestParam("version") String version) {
        Yaml yaml = new Yaml();
        HelmResultVo helmResultVo = helmOperateService.showValues(name, version);
        List<String> list = (List<String>) helmResultVo.getResult();
        InputStream inputStream = new ByteArrayInputStream(String.join(System.lineSeparator(), list).getBytes(StandardCharsets.UTF_8));
        Map map = yaml.loadAs(inputStream, Map.class);
        System.out.println(map.get("image"));
        return R.ok(map);
    }

    /**
     * 下载
     */
    @GetMapping("/download")
    public R<Object> download(@RequestParam("name") String name, @RequestParam("version") String version, HttpServletRequest request, HttpServletResponse response) {
        HelmResultVo helmResultVo = helmOperateService.showValues(name, version);
        File file = FileUtil.createTempFile();
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(helmResultVo.getResult().toString());
            // 清空response
            response.reset();
            // 设置response的Header
            response.addHeader("Content-Disposition", "attachment;filename=" + file.getName());
            response.addHeader("Content-Length", String.valueOf(file.length()));
            InputStream inputStream = FileUtil.getInputStream(file);
            ServletOutputStream servletOutputStream =  response.getOutputStream();
            IoUtil.copy(inputStream, servletOutputStream);
            inputStream.close();
            servletOutputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return R.ok();
    }
}
