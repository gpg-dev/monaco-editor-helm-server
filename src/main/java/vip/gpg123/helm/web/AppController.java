package vip.gpg123.helm.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.gpg123.helm.domain.App;
import vip.gpg123.helm.domain.HelmResultVo;
import vip.gpg123.helm.service.AppService;
import vip.gpg123.helm.service.HelmOperateService;
import vip.gpg123.helm.util.R;

import java.util.List;
import java.util.Map;

/**
 * @author gaopuguang
 * @date 2023/8/4 16:41
 **/
@RestController
@RequestMapping("/app")
public class AppController {

    @Autowired
    private AppService appService;

    @Autowired
    private HelmOperateService helmOperateService;

    /**
     * page
     *
     * @return
     */
    @GetMapping("/page")
    public R<List<App>> getPage() {
        return R.ok(appService.list());
    }

    /**
<<<<<<< HEAD
     * 部署
=======
>>>>>>> origin/master
     * @param map
     * @return
     */
    @PostMapping("/deploy")
    public R<Boolean> deploy(@RequestBody Map<String, String> map) {
        String id = map.get("id");
        App app = new App();
        app.setId(Long.valueOf(id));
        app.setStatus(1);
        appService.updateById(app);
        try {
            testAsyncTask3(id);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return R.ok(true);
    }

    /**
     * 使用自定义线程池异步方法
     *
     * @return void
     * @author Liangyixiang
     * @date 2021/11/14
     **/
    @Async("customExecutor")
    public void testAsyncTask3(String id) throws InterruptedException {
        System.out.println("内部线程：" + Thread.currentThread().getName());
        System.out.println("开始执行任务2");
        long start = System.currentTimeMillis();
        App app = new App();
        app.setId(Long.valueOf(id));
        try {
            HelmResultVo vo = helmOperateService.helmRepoUpdate("");
            if (vo.getExitCode() == 1){
                app.setStatus(3);
            }else if (vo.getExitCode() == 0){
                app.setStatus(2);
            }
        } catch (Exception e) {
            app.setStatus(3);
            throw new RuntimeException(e);
        }
        // 完成后更新状态
        appService.updateById(app);
        // 发送ws
        long end = System.currentTimeMillis();
        System.out.println("完成任务二，耗时：" + (end - start) + "毫秒");
    }

}
