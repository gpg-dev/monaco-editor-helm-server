package vip.gpg123.helm;

import cn.hutool.cron.task.RunnableTask;
import cn.hutool.json.JSONObject;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author gaopuguang
 * @date 2023/8/8 8:43
 **/
public class Nexus3Test {

    private static final String NEXUS_USERNAME = "admin";
    private static final String NEXUS_PASSWORD = "gaopuguang@2023";
    private static final String API_URL = "http://localhost:8081/service/rest/v1/components";
    private static final String encode = Base64.getEncoder().encodeToString((NEXUS_USERNAME + ":" + NEXUS_PASSWORD).getBytes(StandardCharsets.UTF_8));

    /**
     * 使用rest-api进行upload
     *
     * @throws IOException
     */
    @Test
    public void testUploadByRestApi() throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("helm.asset", "D:\\app-1-0.1.0.tgz",
                        RequestBody.create(MediaType.parse("application/octet-stream"),
                                new File("D:\\app-1-0.1.0.tgz")))
                .build();
        Request request = new Request.Builder()
                .url(API_URL + "?repository=helm-repo")
                .method("POST", body)
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
                .addHeader("Authorization", "Basic " + encode)
                .addHeader("Content-Length", "")
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        System.out.println(response);
        System.out.println(response);
    }

    /**
     * 使用rest-api获取所有
     */
    @Test
    public void testGetAll() throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(API_URL + "?repository=helm-repo")
                .method("GET", null)
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "Basic " + encode)
                .build();
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        System.out.println(response.body().string());
    }

    /**
     * 使用rest-api获取单个信息
     */
    @Test
    public void testGetById() throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        String componentId = "aGVsbS1yZXBvOmM2MGJjNmI5NjEyZjQ3ZDM5ZTc2ZmMwNTI1ODg0M2Rj";
        Request request = new Request.Builder()
                .url(API_URL + "/" + componentId)
                .method("GET", null)
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "Basic " + encode)
                .build();
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        System.out.println(response.body().string());
    }

    /**
     * 使用rest-api进行delete
     */
    @Test
    public void testDeleteByRestApi() throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        String componentId = "aGVsbS1yZXBvOjIyYjM2ODZhOTA2NGZhM2Q2ZDA5NDBkNGI3OTY3ZGI0";
        Request request = new Request.Builder()
                .url(API_URL + "/" + componentId)
                .method("DELETE", null)
                //.addHeader("User-Agent", "apifox/1.0.0 (https://www.apifox.cn)")
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "Basic " + encode)
                .build();
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        System.out.println(response);
    }

    @Test
    public String getChartIdByName() throws IOException {
        String name = "argo-cd11";
        String version = "5.43.3";
        String fileId = null;
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("http://localhost:8081/service/rest/v1/search?repository=helm-repo&name=" + name + "&version=" + version)
                .method("GET", null)
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "Basic " + encode)
                .build();
        Response response = client.newCall(request).execute();

        if (response.code() == HttpStatus.OK.value()) {
            assert response.body() != null;
            Map map = jsonStringToMap(response);
            JSONObject jsonObject = new JSONObject(map);
            if (jsonObject.getJSONArray("items").size() == 0) {
                System.out.println("为0");
                getChartFileIdCallback();
            } else {
                fileId = jsonObject.getJSONArray("items").getJSONObject(0).get("id").toString();
                System.out.println(fileId);
                return fileId;
            }
        } else if (response.code() == HttpStatus.NOT_FOUND.value()) {
            throw new RuntimeException("操作失败，" + response.message() + "找不到此名称的chart");
        } else {
            throw new RuntimeException("操作失败，未知原因");
        }
        return null;
    }

    @Test
    public void testGetChart() {
        Thread thread = new Thread(() -> {
            try {
                Thread.sleep(3000);
                String fileId = getChartFileIdCallback();
                System.out.println("fileId:" + fileId);
            } catch (InterruptedException | IOException e) {
                throw new RuntimeException(e);
            }
        });
        thread.start();
        try {
            // 超时5s
            thread.join(5000);
            if (thread.isAlive()) {
                System.out.println("没有查到");
            } else {
                System.out.println("查到了");
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testGet(){

    }

    @Test
    public void job() throws InterruptedException {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.schedule(() -> {
            try {
                getChartFileIdCallback();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }, 1, TimeUnit.SECONDS);

    }

    @Test
    public void asyncRefreshChart() {
        TimerTask task = new TimerTask() {
            /**
             * The action to be performed by this timer task.
             */
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("测试");
            }
        };
        final String[] fileId = new String[1];
        // 创建timer对象
        Timer timer = new Timer();
        // 设定定时2s
        timer.schedule(task, 1000L, 2000L);
    }

    private String getChartFileIdCallback() throws IOException {
        String name = "argo-cd11";
        String version = "5.43.3";
        String fileId = null;
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("http://localhost:8081/service/rest/v1/search?repository=helm-repo&name=" + name + "&version=" + version)
                .method("GET", null)
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "Basic " + encode)
                .build();
        Response response = client.newCall(request).execute();

        if (response.code() == HttpStatus.OK.value()) {
            assert response.body() != null;
            Map map = jsonStringToMap(response);
            JSONObject jsonObject = new JSONObject(map);
            if (jsonObject.getJSONArray("items").size() == 0) {
                System.out.println("为0");
                getChartFileIdCallback();
            } else {
                fileId = jsonObject.getJSONArray("items").getJSONObject(0).get("id").toString();
                System.out.println(fileId);
                return fileId;
            }
        } else if (response.code() == HttpStatus.NOT_FOUND.value()) {
            throw new RuntimeException("操作失败，" + response.message() + "找不到此名称的chart");
        } else {
            throw new RuntimeException("操作失败，未知原因");
        }
        return fileId;
    }


    /**
     * json字符转map
     *
     * @param response 响应
     * @return map
     */
    private Map jsonStringToMap(Response response) {
        assert response.body() != null;
        ResponseBody responseBody = response.body();
        Yaml yaml = new Yaml();
        return yaml.loadAs(responseBody.byteStream(), Map.class);
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException, TimeoutException {
//        TimerTask task = new TimerTask() {
//            @Override
//            public void run() {
//                //Thread.sleep(1000);
//                System.out.println("测试");
//            }
//        };
//        // 创建timer对象
//        Timer timer = new Timer();
//        // 设定定时2s
//        timer.schedule(task, 1000L, 2000L);
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        Runnable taskRunnable = new Runnable(){
            @Override
            public void run() {
               Nexus3Test nt = new Nexus3Test();
                try {
                    nt.getChartIdByName();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        ScheduledFuture<?> future = executor.schedule(taskRunnable,2L,TimeUnit.SECONDS);
        future.get(5L,TimeUnit.SECONDS);
    }

}
