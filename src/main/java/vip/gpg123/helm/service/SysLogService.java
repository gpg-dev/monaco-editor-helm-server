package vip.gpg123.helm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.gpg123.helm.domain.SysLog;

/**
 * @author gaopuguang
 * @date 2023/7/25 10:44
 **/
public interface SysLogService extends IService<SysLog> {
}
