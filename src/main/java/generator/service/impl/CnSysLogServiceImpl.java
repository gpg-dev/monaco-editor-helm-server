package generator.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import generator.domain.CnSysLog;
import generator.service.CnSysLogService;
import generator.mapper.CnSysLogMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【cn_sys_log(系统日志表)】的数据库操作Service实现
* @createDate 2023-09-27 14:39:01
*/
@Service
public class CnSysLogServiceImpl extends ServiceImpl<CnSysLogMapper, CnSysLog>
    implements CnSysLogService{

}




