package vip.gpg123.helm.mapper;

import org.apache.ibatis.annotations.Mapper;
import vip.gpg123.helm.domain.App;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【app】的数据库操作Mapper
* @createDate 2023-08-04 16:40:18
* @Entity vip.gpg123.helm.domain.App
*/
@Mapper
public interface AppMapper extends BaseMapper<App> {

}




