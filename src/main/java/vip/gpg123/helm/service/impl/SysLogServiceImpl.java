package vip.gpg123.helm.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.gpg123.helm.domain.SysLog;
import vip.gpg123.helm.mapper.SysLogMapper;
import vip.gpg123.helm.service.SysLogService;

/**
 * @author gaopuguang
 * @date 2023/7/25 10:46
 **/
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {

}
