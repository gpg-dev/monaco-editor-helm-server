package generator.mapper;

import generator.domain.CnSysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【cn_sys_log(系统日志表)】的数据库操作Mapper
* @createDate 2023-09-27 14:39:01
* @Entity generator.domain.CnSysLog
*/
public interface CnSysLogMapper extends BaseMapper<CnSysLog> {

}




