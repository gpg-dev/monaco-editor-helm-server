package vip.gpg123.helm;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.junit.jupiter.api.Test;
import org.w3c.dom.NodeList;
import org.yaml.snakeyaml.Yaml;
import vip.gpg123.helm.util.FileTreeUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author gaopuguang_zz
 * @version 1.0
 * @description: TODO
 * @date 2023/8/30 14:51
 */
public class FileTest {

    /**
     * 获取目录
     *
     * @throws IOException e
     */
    @Test
    public void testGetFileContent() throws IOException {
        // 目录
        String contentPath = "D:\\data\\micro-app-backend\\0\\demo";
        char chart = File.separatorChar;
        // 0表示最顶层的id是0
        List<File> fileList = new ArrayList<>();
        file(contentPath, fileList);
        System.out.println(fileList.size());

        List<TreeNode<String>> nodeList = new ArrayList<>();

        for (int i = 0; i < fileList.size(); i++) {
            File file = fileList.get(i);
            //
            String filePath = fileList.get(i).getPath();
            int contentCount = countChar(contentPath, chart);
            int fileCount = countChar(filePath, chart);
            // 扩展字段
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("path", file.getPath());
            map.put("absolutePath", file.getAbsolutePath());
            map.put("parent", file.getParent());
            map.put("canWrite", file.canWrite());
            map.put("canRead", file.canRead());
            map.put("isFile", file.isFile());
            map.put("lastModified", DateUtil.date(file.lastModified()).toString());
            map.put("isHidden", file.isHidden());
            map.put("usableSpace", file.getUsableSpace());
            map.put("freeSpace", file.getFreeSpace());
            map.put("totalSpace", file.getTotalSpace());
            // 构造tree
            TreeNode<String> tree = new TreeNode<>();
            tree.setParentId(String.valueOf(fileCount - contentCount - 1));
            tree.setId(String.valueOf(i));
            tree.setName(fileList.get(i).getName());
            tree.setWeight(file.isFile() ? 0 : 1);
            tree.setExtra(map);
            nodeList.add(tree);
            //
            System.out.println(fileList.get(i).getPath());
        }
        List<Tree<String>> treeList = TreeUtil.build(nodeList, "-1");
        String jsonStr = JSONUtil.toJsonStr(treeList);
        String jsonStrFmt = JSONUtil.formatJsonStr(jsonStr);
        System.out.println(jsonStrFmt);
        File file = new File("D:/example2.json");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(jsonStrFmt);
        fileWriter.close();
    }

    /**
     * file内容转数组
     */
    @Test
    public void getFileByline() {
        String filePath = "D:\\project\\server\\monaco-editor-helm-server\\src\\main\\resources\\application.yml";
        BufferedReader br = null;
        List<String> list = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(filePath));
            String str = null;
            while ((str = br.readLine()) != null) {
                list.add(str);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        list.forEach(System.out::println);
        String json = JSONUtil.toJsonStr(list);
        System.out.println(list);
    }

    /**
     * list数组转yaml
     */
    @Test
    public void arrToYaml() throws IOException {
        List<String> list = new ArrayList<>();
        list.add("server:");
        list.add("  port: 8080");
        list.add("spring:");
        list.add("  profiles:");
        list.add("    active: dev");
        Yaml yaml = new Yaml();
        File file = new File("D://arrToYamlTest.yaml");
        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);
        StringBuilder str = new StringBuilder();
        for (String line : list) {
            bw.write(line + "\n");
            str.append(line + "nn");
        }
        bw.close();
        fw.close();
        System.out.println(str);
        //
        String jsonStr1 = JSONUtil.toJsonStr(list);
        System.out.println(jsonStr1);
        //
        String yamlContent = new String(Files.readAllBytes(Paths.get(file.toURI())));
        System.out.println(yamlContent);
        // 转map
        Map map = yaml.load(FileUtil.getInputStream(file));
        System.out.println(map);
        // 转json字符
        String jsonStr2 = JSONUtil.toJsonStr(map);
        System.out.println(jsonStr2);
        //
        String yamlStr = yaml.dump(jsonStr2);
        System.out.println(yamlStr);

        Map map2 = yaml.load((InputStream) list);
        System.out.println(map2);
    }

    /**
     * yaml格式化
     */
    @Test
    public void yamlFormat() {
        File file = new File("D:\\project\\server\\monaco-editor-helm-server\\src\\main\\resources\\application.yml");
        InputStream inputStream = IoUtil.toStream(file);
        Yaml yaml = new Yaml();
        String str = yaml.load(inputStream);
        System.out.println(str);
    }

    /**
     * json字符串转数组
     */
    @Test
    public void jsonStrToArr() {
        String jsonString = "[{\"name\":\"Tom\",\"age\":18},{\"name\":\"Jack\",\"age\":20}]";
        JSONArray jsonArray = JSONUtil.parseArray(jsonString);
        String[] stringArray = jsonArray.toArray(new String[jsonArray.size()]);
        //System.out.println(stringArray);
        for (String line : stringArray) {
            System.out.println(line);
        }
    }

    @Test
    public void fileTree() {
        //扫描此文件夹下面的所有文件
        String filepath = "D:\\data\\micro-app-backend\\0\\demo";
        //初始化父节点id
        int parentId = 0;
        try {
           FileTreeUtil.file(filepath, parentId);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
//        for (int i = 0; i < list.size(); i++) {
//            vip.gpg123.helm.util.Tree tree = list.get(i);
//            System.out.println("id==  " + tree.getId() + "  parentId==  " + tree.getParentId() + "  url==  " + tree.getPath());
//        }
    }

    /**
     * json字符转map
     */
    @Test
    public void jsonStrToMap() {
        File file = new File("D:\\project\\server\\monaco-editor-helm-server\\src\\main\\resources\\commonApp.json");
        JSONObject jsonObject = JSONUtil.readJSONObject(file, CharsetUtil.CHARSET_UTF_8);
        String jsonString = jsonObject.toString();
        //String jsonString = "{\"name\":\"Tom\",\"age\":18}";
        //String jsonString = "[{\"name\":\"Tom\",\"age\":18},{\"name\":\"Jack\",\"age\":20}]";
        System.out.println("是否json：" + JSONUtil.isTypeJSON(jsonString));
        System.out.println("是否jsonArr：" + JSONUtil.isTypeJSONArray(jsonString));
        System.out.println("是否json对象：" + JSONUtil.isTypeJSONObject(jsonString));
        //JSONArray jsonArray = JSONUtil.parseArray(jsonString);
        //System.out.println(jsonArray);

        //JSONObject jsonObject2 = JSONUtil.parseObj(jsonArray);
        //System.out.println(jsonObject);

        //json对象转map
        Map<String, Object> map = jsonObject;
        System.out.println(map);
        map.forEach((k, v) -> {
            //System.out.println("k:" + k + ",v:" + v);
            System.out.println(k + "=" + v);
        });

    }

    public List<File> file(String contentPath, List<File> files) throws FileNotFoundException {
        File file = new File(contentPath);
        //files.add(file);
        //1.判断文件
        if (!file.exists()) {
            throw new FileNotFoundException("文件不存在");
        }
        //2.文件
        if (file.isFile()) {
            files.add(file);
        }
        //3.获取文件夹路径下面的所有文件递归调用；
        if (file.isDirectory()) {
            files.add(file);
            String path = file.getAbsolutePath();
            String[] list = file.list();
            for (int i = 0; i < list.length; i++) {
                String s = list[i];
                //根据当前文件夹，拼接其下文文件形成新的路径
                String newFilePath = path + "\\" + s;
                //files.add(file);
                file(newFilePath, files);
            }
        }
        return files;
    }

    public static int countChar(String str, char c) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c) {
                count++;
            }
        }
        return count;
    }

    @Test
    public void testFile() {
        String contentPath = "D:\\data\\micro-app-backend\\0\\demo";
        File file = new File(contentPath);
        //System.out.println(file);
        List<File> fileList = FileUtil.loopFiles(contentPath);
        fileList.forEach(f -> {
            System.out.println(f.getName());
        });

        List<TreeNode<String>> nodeList = new ArrayList<>();
        String[] files = file.list();
    }

//    public void xunHuan(File file,List<TreeNode<String>> nodeList) {
//        // 文件不遍历
//        if (file.isFile()){
//            // 构造tree
//            TreeNode<String> tree = new TreeNode<>();
//            //tree.setId();
//            tree.setParentId(String.valueOf(fileCount - contentCount - 1));
//            tree.setName(file.getName());
//            tree.setWeight(file.isFile() ? 0 : 1);
//            tree.setExtra(map);
//            nodeList.add(tree);
//        }else {
//            //文件夹则
//
//        }
//    }


    @Test
    public void fileToTreeTest() throws IOException {
        String path = "D:\\data\\micro-app-backend\\0\\demo";
        List<TreeNode<String>> nodeList = new ArrayList<>();
        TreeNode<String> init = genDirExtra(0, new File(path));
        nodeList.add(init);
        genDirTree(path, 0, nodeList);
        List<Tree<String>> treeList = TreeUtil.build(nodeList, String.valueOf(-1));
        String jsonStr = JSONUtil.toJsonStr(treeList);
        String jsonStrFmt = JSONUtil.formatJsonStr(jsonStr);
        System.out.println(jsonStrFmt);
        File file = new File("D:\\0928.json");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(jsonStrFmt);
        fileWriter.close();
    }


    public static void genDirTree(String path, int level, List<TreeNode<String>> nodeList) {
        level++;
        File file = new File(path);
        File[] files = file.listFiles();
        if (!file.exists()) {
            System.out.println("文件不存在");
            return;
        }
        if (files.length != 0) {
            //
            for (File f : files) {
                String dir = f.getName();
                String st = levelSign(level) + dir;
                System.out.println(st);
                TreeNode<String> tree = genDirExtra(level, f);
                nodeList.add(tree);
                if (f.isDirectory()) {
                    genDirTree(f.getAbsolutePath(), level, nodeList);
                } else {
                    System.out.println(levelSign(level) + f.getName());
                }
            }
        }
    }

    //文件层级信息
    private static String levelSign(int level) {
        StringBuilder sb = new StringBuilder();
        sb.append(" ├─");
        for (int x = 0; x < level; x++) {
            sb.insert(0, " │   ");
        }
        return sb.toString();
    }

    //
    private static TreeNode<String> genDirExtra(int level, File f) {
        // 扩展字段
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("path", f.getPath());
        map.put("absolutePath", f.getAbsolutePath());
        map.put("parent", f.getParent());
        map.put("canWrite", f.canWrite());
        map.put("canRead", f.canRead());
        map.put("isFile", f.isFile());
        map.put("lastModified", DateUtil.date(f.lastModified()).toString());
        map.put("isHidden", f.isHidden());
        map.put("usableSpace", f.getUsableSpace());
        map.put("freeSpace", f.getFreeSpace());
        map.put("totalSpace", f.getTotalSpace());
        // 构造tree
        TreeNode<String> tree = new TreeNode<>();
        tree.setId(String.valueOf(level));
        tree.setParentId(String.valueOf(level - 1));
        tree.setName(f.getName());
        tree.setWeight(f.isFile() ? 1 : 0);
        tree.setExtra(map);
        return tree;
    }
}
