package vip.gpg123.helm.domain;

import java.util.List;

/**
 * @author gaopuguang
 * @date 2023/7/19 17:48
 **/
public class TreeNode {

    private Integer id;
    private String title;
    private String key;
    private String path;
    private String parentPath;
    private boolean isLeaf;
    private List<TreeNode> children;

    public TreeNode() {
    }

    public TreeNode(Integer id, String title, String key, String path, String parentPath, boolean isLeaf, List<TreeNode> children) {
        this.id = id;
        this.title = title;
        this.key = key;
        this.path = path;
        this.parentPath = parentPath;
        this.isLeaf = isLeaf;
        this.children = children;
    }

    public String getParentPath() {
        return parentPath;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean getIsLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(boolean isLeaf) {
        this.isLeaf = isLeaf;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }
}
