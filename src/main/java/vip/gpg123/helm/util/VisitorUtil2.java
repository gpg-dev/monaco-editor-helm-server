package vip.gpg123.helm.util;

/**
 * @author gaopuguang_zz
 * @version 1.0
 * @description: TODO
 * @date 2023/10/7 9:05
 */

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.PathUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.json.JSONUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.nio.file.FileVisitResult.CONTINUE;

public class VisitorUtil2 {

    public static void main(String[] args) throws IOException {
        String contentPath = "D:\\data\\micro-app-backend\\0\\csContent";
        Path startDir = Paths.get(contentPath);
        //
        List<TreeNode<String>> nodeList = new ArrayList<>();
        int rootLevel = countChar(contentPath, File.separatorChar);
        FileVisitor<Path> visitor = getFileVisitor(nodeList, rootLevel);
        try {
            Files.walkFileTree(startDir, visitor);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(nodeList);
        List<Tree<String>> treeList = TreeUtil.build(nodeList, "-1");
        String jsonStr = JSONUtil.toJsonStr(treeList);
        String jsonStrFmt = JSONUtil.formatJsonStr(jsonStr);
        System.out.println(jsonStrFmt);
        File file = new File("D:/example3.json");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(jsonStrFmt);
        fileWriter.close();
    }

    public static FileVisitor<Path> getFileVisitor(List<TreeNode<String>> nodeList, int rootLevel) {
        class DirVisitor<Path> extends SimpleFileVisitor<Path> {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
                buildTreeNodeList(nodeList, rootLevel, dir.toString());
                return CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                buildTreeNodeList(nodeList, rootLevel, file.toString());
                return CONTINUE;
            }
        }
        FileVisitor<Path> visitor = new DirVisitor<>();
        return visitor;
    }

    // 计算层级
    public static int countChar(String str, char c) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c) {
                count++;
            }
        }
        return count;
    }

    // 构建树
    public static void buildTreeNodeList(List<TreeNode<String>> nodeList, int rootLevel, String path) {
        int directoryLevel = countChar(path, File.separatorChar);
        int parentId = directoryLevel - rootLevel;
        File file = new File(path);
        File parent = new File(file.getParent());
        // 扩展字段
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("path", file.getPath());
        map.put("absolutePath", file.getAbsolutePath());
        map.put("parent", file.getParent());
        map.put("canWrite", file.canWrite());
        map.put("canRead", file.canRead());
        map.put("isFile", file.isFile());
        map.put("lastModified", DateUtil.date(file.lastModified()).toString());
        map.put("isHidden", file.isHidden());
        map.put("usableSpace", file.getUsableSpace());
        map.put("freeSpace", file.getFreeSpace());
        map.put("totalSpace", file.getTotalSpace());
        map.put("length", file.length());
        map.put("fileType", file.isFile() ? FileUtil.extName(file) : "dir");
        // 构建tree
        TreeNode<String> tree = new TreeNode<>();
        tree.setId(parentId + "-" + file.getName());
        if (parentId == 0) {
            tree.setParentId("-1");
        } else {
            tree.setParentId(parentId - 1 + "-" + parent.getName());
        }
        tree.setName(file.getName());
        tree.setWeight(file.isFile() ? 1 : 0);
        tree.setExtra(map);
        nodeList.add(tree);
    }
}
