create table cscp_gov_update_records
(
    id          bigint auto_increment comment 'id主键'
        primary key
        unique,
    service_id  varchar(64)                       null comment '服务实例id',
    params      text                              null comment '参数',
    state       tinyint(1)                        null comment '状态',
    type        tinyint(1)                        null comment '类型，1-黑白，2-限流，3-熔断，4-负载，5-超时',
    create_time datetime   default localtimestamp null,
    update_time datetime   default localtimestamp null,
    create_by   varchar(64)                       null,
    update_by   varchar(64)                       null,
    del_flag    tinyint(1) default 0              null
);

