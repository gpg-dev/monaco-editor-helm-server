package generator.service;

import generator.domain.CnSysLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【cn_sys_log(系统日志表)】的数据库操作Service
* @createDate 2023-09-27 14:39:01
*/
public interface CnSysLogService extends IService<CnSysLog> {

}
