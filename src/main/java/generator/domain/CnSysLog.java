package generator.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 系统日志表
 * @TableName cn_sys_log
 */
@TableName(value ="cn_sys_log")
@Data
public class CnSysLog implements Serializable {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 日志类型
     */
    @TableField(value = "log_type")
    private String logType;

    /**
     * 消息内容
     */
    @TableField(value = "message")
    private String message;

    /**
     * 创建用户id
     */
    @TableField(value = "create_by")
    private String createBy;

    /**
     * 创建用户名称
     */
    @TableField(value = "create_user_name")
    private String createUserName;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 请求URI
     */
    @TableField(value = "request_uri")
    private String requestUri;

    /**
     * 请求方式
     */
    @TableField(value = "request_method")
    private String requestMethod;

    /**
     * 请求参数
     */
    @TableField(value = "request_params")
    private String requestParams;

    /**
     * 请求IP
     */
    @TableField(value = "request_ip")
    private String requestIp;

    /**
     * 请求服务器地址
     */
    @TableField(value = "server_address")
    private String serverAddress;

    /**
     * 是否异常
     */
    @TableField(value = "is_exception")
    private String isException;

    /**
     * 异常信息
     */
    @TableField(value = "exception_info")
    private String exceptionInfo;

    /**
     * 开始时间
     */
    @TableField(value = "start_time")
    private Date startTime;

    /**
     * 结束时间
     */
    @TableField(value = "end_time")
    private Date endTime;

    /**
     * 执行时间
     */
    @TableField(value = "execute_time")
    private Integer executeTime;

    /**
     * 用户代理
     */
    @TableField(value = "user_agent")
    private String userAgent;

    /**
     * 操作系统
     */
    @TableField(value = "device_name")
    private String deviceName;

    /**
     * 浏览器名称
     */
    @TableField(value = "browser_name")
    private String browserName;

    /**
     * 是否删除
     */
    @TableField(value = "del_flag")
    private Integer delFlag;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}