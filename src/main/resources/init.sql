create table sys_log
(
    id               bigint auto_increment comment 'ID'
        primary key,
    log_type         varchar(50)          not null comment '日志类型',
    create_user_id   varchar(64)          not null comment '创建用户id',
    create_user_name varchar(100)         not null comment '创建用户名称',
    create_date      datetime             not null comment '创建时间',
    request_uri      varchar(500)         null comment '请求URI',
    request_method   varchar(10)          null comment '请求方式',
    request_params   text                 null comment '请求参数',
    request_ip       varchar(20)          not null comment '请求IP',
    server_address   varchar(50)          not null comment '请求服务器地址',
    is_exception     char                 null comment '是否异常',
    exception_info   text                 null comment '异常信息',
    start_time       datetime             not null comment '开始时间',
    end_time         datetime             not null comment '结束时间',
    execute_time     int                  null comment '执行时间',
    user_agent       varchar(500)         null comment '用户代理',
    device_name      varchar(100)         null comment '操作系统',
    browser_name     varchar(100)         null comment '浏览器名称',
    message          longtext             null,
    del_flag         tinyint(1) default 0 null
)
    comment '系统日志表';

create index idx_sys_log_cd
    on sys_log (create_date);

create index idx_sys_log_cub
    on sys_log (create_user_id);

create index idx_sys_log_ie
    on sys_log (is_exception);

create index idx_sys_log_lt
    on sys_log (log_type);

