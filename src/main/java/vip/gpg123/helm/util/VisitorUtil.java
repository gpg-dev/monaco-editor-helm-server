package vip.gpg123.helm.util;

/**
 * @author gaopuguang_zz
 * @version 1.0
 * @description: TODO
 * @date 2023/10/7 9:05
 */

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.json.JSONUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.FileVisitResult.CONTINUE;

public class VisitorUtil {

    public static List<TreeNode<String>> nodeList = new ArrayList<>();
    public static String contentPath = "D:\\data\\micro-app-backend\\0\\demo";
    public static int level = countChar(contentPath, File.separatorChar);

    public static void main(String[] args) throws IOException {
        Path startDir = Paths.get(contentPath);
        //
        int rootParentId = 0;
        FileVisitor<Path> visitor = getFileVisitor();
        try {
            Files.walkFileTree(startDir, visitor);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(nodeList);
        List<Tree<String>> treeList = TreeUtil.build(nodeList, "-1");
        String jsonStr = JSONUtil.toJsonStr(treeList);
        String jsonStrFmt = JSONUtil.formatJsonStr(jsonStr);
        System.out.println(jsonStrFmt);
        File file = new File("D:/example3.json");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(jsonStrFmt);
        fileWriter.close();
    }

    public static FileVisitor<Path> getFileVisitor() {
        class DirVisitor<Path> extends SimpleFileVisitor<Path> {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
                System.out.format("%s [Directory]%n", dir);
                int directoryLevel = countChar(dir.toString(), File.separatorChar);
                int parentId = directoryLevel - level;
                File file = new File(dir.toString());
                File parent = new File(file.getParent());
                TreeNode<String> tree = new TreeNode<>();
                tree.setId(parentId + "-" + file.getName());
                if (parentId == 0) {
                    tree.setParentId("-1");
                } else {
                    tree.setParentId(parentId-1 + "-" + parent.getName());
                }
                tree.setName(file.getName());
                nodeList.add(tree);
                return CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                System.out.format("%s [File,  Size: %s  bytes]%n", file, attrs.size());
                int directoryLevel = countChar(file.toString(), File.separatorChar);
                int parentId = directoryLevel - level;
                File file1 = new File(file.toString());
                File parent = new File(file1.getParent());
                TreeNode<String> tree = new TreeNode<>();
                tree.setId(parentId + "-" + file1.getName());
                tree.setParentId(parentId-1 + "-" + parent.getName());
                tree.setName(file1.toString());
                nodeList.add(tree);
                return CONTINUE;
            }
        }
        FileVisitor<Path> visitor = new DirVisitor<>();
        return visitor;
    }


    public static int countChar(String str, char c) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c) {
                count++;
            }
        }
        return count;
    }


}
