package vip.gpg123.helm.service;

import vip.gpg123.helm.domain.App;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【app】的数据库操作Service
* @createDate 2023-08-04 16:40:18
*/
public interface AppService extends IService<App> {

}
