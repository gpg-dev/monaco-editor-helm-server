package vip.gpg123.helm.util;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

/**
 * @author Administrator
 */
public class YamlStyleUtil {

    public static Yaml defaultYaml(Class<?> clazz){
        //
        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);//通常使用的yaml格式
        options.setDefaultScalarStyle(DumperOptions.ScalarStyle.PLAIN);//标量格式
        options.setIndent(1);

        //
        Representer representer = new Representer();
        representer.addClassTag(clazz, Tag.MAP);

        return new Yaml(representer,options);
    }

    /**
     *
     * @return
     */
    public static DumperOptions defaultStyle (){
        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);//通常使用的yaml格式
        options.setDefaultScalarStyle(DumperOptions.ScalarStyle.PLAIN);//标量格式
        options.setIndent(1);

        //options.setPrettyFlow(true);
        return options;
    }
}
