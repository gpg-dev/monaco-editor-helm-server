package vip.gpg123.helm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Administrator
 */
@SpringBootApplication
public class HelmApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelmApiApplication.class, args);
    }

}
