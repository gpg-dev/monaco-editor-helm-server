package vip.gpg123.helm.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.json.JSONObject;
import lombok.extern.slf4j.Slf4j;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.yaml.snakeyaml.Yaml;
import vip.gpg123.helm.service.ChartService;
import vip.gpg123.helm.util.R;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileSystemException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author gaopuguang_zz
 * @version 1.0
 * @description: TODO
 * @date 2023/8/15 11:00
 */
@Service
@Slf4j
public class ChartServiceImpl implements ChartService {

    @Value("${repository.nexus3.api}")
    private String api;

    @Value("${repository.nexus3.repoName}")
    private String repoName;

    @Value("${repository.nexus3.encode}")
    private String encode;

    private static final String EXTENSION = ".tgz";

    /**
     * 上传chart文件
     *
     * @param file chart文件
     */
    @Override
    public R<Boolean> uploadChart(MultipartFile file) throws IOException {
        if (file.getSize() == 0) {
            throw new FileSystemException("无效文件，文件大小为0");
        }
        String originalFilename = file.getOriginalFilename();
        if (!EXTENSION.equals(originalFilename.substring(originalFilename.lastIndexOf(".")))) {
            throw new FileSystemException("错误格式，非tgz格式文件");
        }

        // MultipartFile转File
        File nfile = new File(originalFilename);
        InputStream is = file.getInputStream();
        OutputStream os = FileUtil.getOutputStream(nfile);
        IoUtil.copy(is, os);
        // 拷贝完后关闭
        is.close();
        os.close();
        // 初始化http客户端
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        // 创建请求体
        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("helm.asset", nfile.getAbsolutePath(),
                        RequestBody.create(MediaType.parse("application/octet-stream"),
                                nfile))
                .build();
        Request request = new Request.Builder()
                .url(api + "/components?repository=" + repoName)
                .method("POST", body)
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "multipart/form-data; charset=utf-8")
                .addHeader("Authorization", "Basic " + encode)
                .addHeader("Content-Length", "")
                .build();

        Response response = client.newCall(request).execute();
        // 删除临时文件
        if (nfile.exists()) {
            FileUtil.del(nfile);
        }
        // 打印状态码
        log.info("上传文件：{},上传结果:{}", file.getOriginalFilename(), response.code());
        // 返回
        if (response.code() == HttpStatus.NO_CONTENT.value()) {
            assert response.body() != null;
            return R.ok(true, "操作成功");
        } else if (response.code() == HttpStatus.BAD_REQUEST.value()) {
            return R.failed(false, "操作失败,已存在相同名称相同版本的chart了");
        } else {
            return R.failed(false, "操作失败,未知原因");
        }
    }

    /**
     * 根据id查询
     *
     * @param id id
     */
    @Override
    public R<Object> searchChartById(String id) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(api + "/components" + id)
                .method("GET", null)
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "Basic " + encode)
                .build();
        Response response = client.newCall(request).execute();
        // 打印状态码
        log.info("查询id：{},查询结果:{}", id, response.code());
        if (response.code() == HttpStatus.OK.value()) {
            assert response.body() != null;
            return R.ok(jsonStringToMap(response), "操作成功");
        } else if (response.code() == HttpStatus.NOT_FOUND.value()) {
            return R.failed(false, "操作失败，" + response.message() + "找不到此id的chart");
        } else {
            return R.failed(response.message(), "操作失败，未知原因");
        }
    }

    /**
     * 根据名称、版本查询
     *
     * @param name 名称
     */
    @Override
    public R<Object> searchChartByName(String name, String version) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(api + "/search?repository=helm-repo&name=" + name + "&version=" + version)
                .method("GET", null)
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "Basic " + encode)
                .build();
        Response response = client.newCall(request).execute();
        // 打印状态码
        log.info("查询名：{}，查询结果：{}", name, response.code());
        if (response.code() == HttpStatus.OK.value()) {
            assert response.body() != null;
            return R.ok(jsonStringToMap(response), "操作成功");
        } else if (response.code() == HttpStatus.NOT_FOUND.value()) {
            return R.failed(false, "操作失败，" + response.message() + "找不到此名称的chart");
        } else {
            return R.failed(response.message(), "操作失败，未知原因");
        }
    }

    /**
     * 根据名称、版本获取id
     *
     * @param name 名称
     */
    @Override
    public R<String> getChartIdByName(String name, String version) throws IOException {
        if (version.isEmpty()) {
          return R.failed("version版本不能为空");
        }
        R<Object> res = searchChartByName(name, version);
        JSONObject jsonObject = null;
        try {
            // 判断是否成功返回，成功就是个map
            if (res.getCode()) {
                // 转map
                jsonObject = new JSONObject(res.getData());
                String id = jsonObject.getJSONArray("items").getJSONObject(0).get("id").toString();
                return R.ok(id, "请求成功");
            }
            throw new RuntimeException("根据名称查询请求不成功");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 查询全部
     */
    @Override
    public R<Object> searchAllChart() throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(api + "/components?repository=" + repoName)
                .method("GET", null)
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "Basic " + encode)
                .build();
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code " + response);
        }
        // 打印状态码
        log.info("获取所有结果:{}", response.code());
        if (response.code() == HttpStatus.OK.value()) {
            return R.ok(jsonStringToMap(response), "操作成功");
        } else {
            return R.failed(response.message(), "操作失败," + response.message());
        }
    }

    /**
     * 根据id删除
     *
     * @param id id
     */
    @Override
    public R<Boolean> deleteChartById(String id) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(api + "/" + id)
                .method("DELETE", null)
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "Basic " + encode)
                .build();
        Response response = client.newCall(request).execute();
        //打印状态码
        log.info("删除id：{}，删除结果：{}", id, response.code());
        if (response.code() == HttpStatus.NO_CONTENT.value()) {
            assert response.body() != null;
            return R.ok(true, "操作成功");
        } else if (response.code() == HttpStatus.NOT_FOUND.value()) {
            return R.failed(false, "操作失败，" + response.message() + "找不到此id的chart");
        } else {
            return R.failed(false, "操作失败，未知原因");
        }
    }


    /**
     * json字符转map
     *
     * @param response 响应
     * @return map
     */
    private Map jsonStringToMap(Response response) {
        assert response.body() != null;
        ResponseBody responseBody = response.body();
        Yaml yaml = new Yaml();
        return yaml.loadAs(responseBody.byteStream(), Map.class);
    }

}
