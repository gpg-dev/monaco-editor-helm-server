package vip.gpg123.helm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.gpg123.helm.domain.App;
import vip.gpg123.helm.service.AppService;
import vip.gpg123.helm.mapper.AppMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【app】的数据库操作Service实现
* @createDate 2023-08-04 16:40:18
*/
@Service
public class AppServiceImpl extends ServiceImpl<AppMapper, App> implements AppService{

}




