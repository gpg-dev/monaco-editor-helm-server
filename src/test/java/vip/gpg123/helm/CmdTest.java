package vip.gpg123.helm;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * @author gaopuguang
 * @date 2023/7/26 16:29
 **/
public class CmdTest {

    @Test
    public void testPb() {
        try {
            //String[] command = {"helm", "list", "-a", "-A", "-o", "json"};
            String[] command = {"helm", "env"};
            ProcessBuilder pb = new ProcessBuilder(command);
            //pb.directory(new File("D:\\1.txt")); // 设置helm所在目录
            Process p = pb.start();

            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains("Error: Kubernetes cluster unreachable: ")) {
                    String error = line.split("Error: Kubernetes cluster unreachable: ")[1].trim();
                    System.out.println(error);
                    break;
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testExecute() {
        String command = "helm list -a ";
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(command);
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            // 获取命令执行的错误输出流
            BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            while ((line = errorReader.readLine()) != null) {
                System.out.println(line);
            }
            int exitCode = process.waitFor();
            System.out.println("the exitCode is :" + exitCode);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testError() {
        try {
            // 创建一个ProcessBuilder对象，传入要执行的cmd命令
            ProcessBuilder processBuilder = new ProcessBuilder("cmd.exe", "/c", "helm list");

            // 启动进程并获取Process对象
            Process process = processBuilder.start();

            // 获取命令执行的错误输出流
            BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            // 读取错误信息
            String line;
            while ((line = errorReader.readLine()) != null) {
                System.out.println("Error: " + line);
            }

            // 等待命令执行完成
            int exitCode = process.waitFor();
            System.out.println("Exit Code: " + exitCode);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
