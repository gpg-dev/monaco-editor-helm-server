package vip.gpg123.helm.mapper;

import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.gpg123.helm.domain.SysLog;


/**
 * 系统日志Dao
 *
 * @author CL
 */
@Mapper
public interface SysLogMapper extends BaseMapper<SysLog> {

}
