create table cscp_helm_template
(
    id               bigint auto_increment
        primary key,
    template_name    varchar(64)                          null comment '草稿名称',
    template_tags    text                                 null comment '标签',
    template_icon    varchar(200)                         null comment '草稿图标',
    template_desc    varchar(200)                         null comment '描述',
    git_app_id       varchar(64)                          null comment '子应用id',
    git_app_name     varchar(64)                          null comment '子应用名',
    version          varchar(20)                          null comment '版本',
    local_path       varchar(200)                         null comment '存放路径',
    create_time      datetime   default CURRENT_TIMESTAMP null,
    update_time      datetime   default CURRENT_TIMESTAMP null,
    create_by        varchar(64)                          null,
    update_by        varchar(64)                          null,
    create_user_name varchar(64)                          null comment '创建者用户名',
    update_user_name varchar(64)                          null comment '更新者用户名',
    del_flag         tinyint(1) default 0                 null comment '是否删除,1-是，0-否',
    constraint id
        unique (id)
)
    comment 'helm草稿表';

create table cscp_sys_log
(
    id               bigint auto_increment comment 'ID'
        primary key,
    log_type         varchar(50)          not null comment '日志类型',
    create_by        varchar(64)          not null comment '创建用户id',
    create_user_name varchar(100)         not null comment '创建用户名称',
    create_time      datetime             not null comment '创建时间',
    request_uri      varchar(500)         null comment '请求URI',
    request_method   varchar(10)          null comment '请求方式',
    request_params   text                 null comment '请求参数',
    request_ip       varchar(20)          not null comment '请求IP',
    server_address   varchar(50)          not null comment '请求服务器地址',
    is_exception     char                 null comment '是否异常',
    exception_info   text                 null comment '异常信息',
    start_time       datetime             not null comment '开始时间',
    end_time         datetime             not null comment '结束时间',
    execute_time     int                  null comment '执行时间',
    user_agent       varchar(500)         null comment '用户代理',
    device_name      varchar(100)         null comment '操作系统',
    browser_name     varchar(100)         null comment '浏览器名称',
    message          longtext             null comment '消息内容',
    del_flag         tinyint(1) default 0 null comment '是否删除'
)
    comment '系统日志表';

