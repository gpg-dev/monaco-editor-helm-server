package vip.gpg123.helm;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.charset.StandardCharsets;

/**
 * @author gaopuguang_zz
 * @version 1.0
 * @description: TODO
 * @date 2023/8/30 10:44
 */
public class JsonTest {

    /**
     * 文件读
     */
    @Test
    public void readJsonFromFile() {
        String filePath = "D:\\project\\server\\monaco-editor-helm-server\\src\\main\\resources\\mysql.json";
        File file = new File(filePath);
        JSONObject jsonObject = JSONUtil.readJSONObject(file, StandardCharsets.UTF_8);
        String jsonStr = jsonObject.toString();
        System.out.println(jsonStr);
        //
        boolean isJson = JSONUtil.isTypeJSON(jsonStr);
        System.out.println(isJson);
        //
        String jsonStrFmt = JSONUtil.formatJsonStr(jsonStr);
        System.out.println(jsonStrFmt);
    }
}
