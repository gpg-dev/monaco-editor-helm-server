package vip.gpg123.helm.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vip.gpg123.helm.domain.HelmResultVo;
import vip.gpg123.helm.service.HelmOperateService;
import vip.gpg123.helm.util.R;

import java.io.IOException;

/**
 * @author gaopuguang
 * @date 2023/7/25 11:00
 **/
@RestController
@RequestMapping("/helm/")
public class HelmOperateController {

    @Autowired
    private HelmOperateService helmOperateService;


    /**
     * 获取版本信息
     */
    @GetMapping("version")
    public R<HelmResultVo> helmVersion() {
        HelmResultVo vo = helmOperateService.getHelmVersion();
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * 环境
     */
    @GetMapping("env")
    public R<HelmResultVo> env() {
        HelmResultVo vo = helmOperateService.helmEnv();
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * 创建
     */
    @PostMapping("create")
    public R<HelmResultVo> helmCreate(@RequestParam("name") String name) {
        HelmResultVo vo = helmOperateService.createHelmPackage(name);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * 查看values
     */
    @GetMapping("showValues")
    public R<HelmResultVo> showValues(@RequestParam("name") String name, @RequestParam("version") String version) {
        HelmResultVo vo = helmOperateService.showValues(name, version);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * json形式查看values
     */
    @GetMapping("showValuesJson")
    public R<HelmResultVo> showValuesJson(@RequestParam("name") String name, @RequestParam("version") String version) throws IOException {
        HelmResultVo vo = helmOperateService.showValuesJson(name, version);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * 查看chart
     */
    @GetMapping("showChart")
    public R<HelmResultVo> showChart(@RequestParam("name") String name, @RequestParam("version") String version) {
        HelmResultVo vo = helmOperateService.showChart(name, version);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * 查看readme
     */
    @GetMapping("readme")
    public R<HelmResultVo> showReadMe(@RequestParam("name") String name, @RequestParam("version") String version) {
        HelmResultVo vo = helmOperateService.showReadMe(name, version);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * 查看crd
     */
    @GetMapping("crd")
    public R<HelmResultVo> showCrd(@RequestParam("name") String name, @RequestParam("version") String version) {
        HelmResultVo vo = helmOperateService.showCrd(name, version);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * 查看全部
     */
    @GetMapping("all")
    public R<HelmResultVo> all(@RequestParam("name") String name, @RequestParam("version") String version) {
        HelmResultVo vo = helmOperateService.showAll(name, version);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * 打包
     */
    @GetMapping("package")
    public R<HelmResultVo> helmPackage(@RequestParam("name") String name) {
        HelmResultVo vo = helmOperateService.package2Tgz(name);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * lint
     */
    @GetMapping("lint")
    public R<HelmResultVo> helmLint(@RequestParam("name") String name) {
        HelmResultVo vo = helmOperateService.helmLint(name);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * template
     */
    @GetMapping("template")
    public R<HelmResultVo> helmTemplate(@RequestParam("name") String name) {
        HelmResultVo vo = helmOperateService.helmTemplate(name);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * install debug
     */
    @GetMapping("debug")
    public R<HelmResultVo> helmDebug(@RequestParam("name") String name) {
        HelmResultVo vo = helmOperateService.helmInstallDebug(name);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * get manifest
     */
    @GetMapping("manifest")
    public R<HelmResultVo> helmManifest(@RequestParam("name") String name, @RequestParam("namespace") String namespace) {
        HelmResultVo vo = helmOperateService.helmManifest(name, namespace);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * get all
     */
    @GetMapping("getAll")
    public R<HelmResultVo> helmGetAll(@RequestParam("releaseName") String releaseName, @RequestParam("namespace") String namespace) {
        HelmResultVo vo = helmOperateService.helmGetAll(releaseName, namespace);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * install
     */
    @PostMapping("install")
    public R<HelmResultVo> helmInstall(@RequestParam("releaseName") String releaseName, @RequestParam("version") String version, @RequestParam("json") String[] json, @RequestParam("name") String name, @RequestParam("namespace") String namespace) {
        HelmResultVo vo = helmOperateService.helmInstall(releaseName, version, json, name, namespace);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * list
     */
    @GetMapping("list")
    public R<HelmResultVo> helmList(@RequestParam("namespace") String namespace, @RequestParam("output") String output) {
        HelmResultVo vo = helmOperateService.helmList(namespace, output);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * status
     */
    @GetMapping("status")
    public R<HelmResultVo> helmStatus(@RequestParam("releaseName") String releaseName, @RequestParam("namespace") String namespace) {
        HelmResultVo vo = helmOperateService.helmStatus(releaseName, namespace);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * history
     */
    @GetMapping("history")
    public R<HelmResultVo> helmHistory(@RequestParam("releaseName") String releaseName, @RequestParam("namespace") String namespace, @RequestParam("output") String output) {
        HelmResultVo vo = helmOperateService.helmHistory(releaseName, namespace, output);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * rollback
     */
    @PostMapping("rollback")
    public R<HelmResultVo> helmRollback(@RequestParam("releaseName") String releaseName, @RequestParam("version") String version, @RequestParam("force") boolean isForce) {
        HelmResultVo vo = helmOperateService.helmRollBack(releaseName, version, isForce);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * update
     */
    @PostMapping("update")
    public R<HelmResultVo> helmUpdate(@RequestParam("releaseName") String releaseName, @RequestParam("values") String[] values, @RequestParam("name") String name, @RequestParam("namespace") String namespace) {
        HelmResultVo vo = helmOperateService.update(releaseName, values, name, namespace);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }


    /**
     * uninstall
     */
    @DeleteMapping("uninstall")
    public R<HelmResultVo> helmUninstall(@RequestParam("releaseName") String releaseName, @RequestParam("namespace") String namespace) {
        HelmResultVo vo = helmOperateService.helmUninstall(releaseName, namespace);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * reo search
     */
    @GetMapping("repoSearch")
    public R<HelmResultVo> repoSearch(@RequestParam(value = "name") String name, @RequestParam("repoName") String repoName, @RequestParam(value = "version", required = false) String version, @RequestParam(value = "output", defaultValue = "json", required = false) String output) {
        HelmResultVo vo = helmOperateService.helmRepoSearch(name, repoName, version, output);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * repo update
     */
    @PostMapping("repoUpdate")
    public R<HelmResultVo> repoUpdate(@RequestParam("repoName") String repoName) {
        HelmResultVo vo = helmOperateService.helmRepoUpdate(repoName);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * repo Add
     */
    @PostMapping("repoAdd")
    public R<HelmResultVo> repoAdd(@RequestParam("name") String name, @RequestParam("url") String url) {
        HelmResultVo vo = helmOperateService.helmRepoAdd(name, url);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * repo list
     */
    @GetMapping("repoList")
    public R<HelmResultVo> repoList(@RequestParam(value = "output", required = false) String output) {
        HelmResultVo vo = helmOperateService.helmRepoList(output);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * repo remove
     */
    @DeleteMapping("repoRemove")
    public R<HelmResultVo> repoRemove(@RequestParam("repoNames") String[] repoNames) {
        HelmResultVo vo = helmOperateService.helmRepoRemove(repoNames);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * plugin list
     */
    @GetMapping("plugin")
    public R<HelmResultVo> pluginList() {
        HelmResultVo vo = helmOperateService.helmPlugin();
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * plugin update
     */
    @PostMapping("pluginUpdate")
    public R<HelmResultVo> pluginUpdate(@RequestParam("pluginNames") String[] pluginNames) {
        HelmResultVo vo = helmOperateService.helmPluginUpdate(pluginNames);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * plugins
     */
    @DeleteMapping("pluginUninstall")
    public R<HelmResultVo> pluginUninstall(@RequestParam("pluginNames") String[] pluginNames) {
        HelmResultVo vo = helmOperateService.helmPluginUninstall(pluginNames);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }

    /**
     * plugins
     */
    @GetMapping("pluginInstall")
    public R<HelmResultVo> pluginInstall(@RequestParam("url") String[] urls) {
        HelmResultVo vo = helmOperateService.helmPluginInstall(urls);
        return vo.getExitCode() == 0 ? R.ok(vo).setMsg(vo.getMessage()) : R.failed(vo).setMsg(vo.getMessage());
    }
}
