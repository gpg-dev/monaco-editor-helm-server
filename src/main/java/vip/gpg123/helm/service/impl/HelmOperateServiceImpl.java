package vip.gpg123.helm.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.Yaml;
import vip.gpg123.helm.domain.HelmResultVo;
import vip.gpg123.helm.service.HelmOperateService;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author gaopuguang
 * @date 2023/6/7 15:54
 **/
@Service
@Slf4j
public class HelmOperateServiceImpl implements HelmOperateService {

    @Value("${repository.local.base-path}")
    private String basePath;

    @Value("${repository.nexus3.helm-repo-name}")
    private String repoName;

    @Value("${repository.nexus3.helm-repo-address}")
    private String repoAddress;

    /**
     * 获取helm-client版本
     */
    @Override
    public HelmResultVo getHelmVersion() {
        String command = "helm version ";
        return executeHelm(command);
    }


    /**
     * 创建helm
     */
    @Override
    public HelmResultVo createHelmPackage(String name) {
        if (name.isEmpty()) {
            throw new RuntimeException("name不能为空");
        }
        // 创建包
        String command = "helm create " + basePath + "/helm/" + name;
        return executeHelm(command);
    }

    /**
     * show values
     * 展示values.yaml文件-输出yaml格式
     */
    @Override
    public HelmResultVo showValues(String name, String version) {
        if (name.isEmpty()) {
            throw new RuntimeException("name不能为空");
        }
        String command;
        // 判断有无指定版本，默认最新版本
        String v = version.isEmpty() ? "" : " --version ";
        command = "helm show values " + repoName + "/" + name + " " + v;
        return executeHelm(command);
    }

    /**
     * show values json
     * 展示values.yaml文件-输出json格式
     */
    @Override
    public HelmResultVo showValuesJson(String name, String version) throws IOException {
        InputStream inputStream = null;
        Yaml yaml = new Yaml();
        // 获取values
        HelmResultVo helmResultVo = showValues(name, version);
        List<String> list = (List<String>) helmResultVo.getResult();
        inputStream = new ByteArrayInputStream(String.join(System.lineSeparator(), list).getBytes(StandardCharsets.UTF_8));
        // 转map
        Map map = yaml.loadAs(inputStream, Map.class);
        helmResultVo.setResult(map);
        inputStream.close();
        return helmResultVo;
    }

    /**
     * show readMe
     * 展示readme.md文件
     */
    @Override
    public HelmResultVo showReadMe(String name, String version) {
        if (name.isEmpty()) {
            throw new RuntimeException("name不能为空");
        }
        String command;
        // 判断有无指定版本，默认最新版本
        String v = version.isEmpty() ? "" : " --version ";
        command = "helm show readme " + repoName + "/" + name + " " + v;
        return executeHelm(command);
    }

    /**
     * show chart
     * 展示chart.yaml
     */
    @Override
    public HelmResultVo showChart(String name, String version) {
        if (name.isEmpty()) {
            throw new RuntimeException("name不能为空");
        }
        String command;
        // 判断有无指定版本，默认最新版本
        String v = version.isEmpty() ? "" : " --version ";
        command = "helm show chart " + repoName + "/" + name + " " + v;
        return executeHelm(command);
    }

    /**
     * show crd
     * 展示crd
     */
    @Override
    public HelmResultVo showCrd(String name, String version) {
        if (name.isEmpty()) {
            throw new RuntimeException("name不能为空");
        }
        String command;
        // 判断有无指定版本，默认最新版本
        String v = version.isEmpty() ? "" : " --version ";
        command = "helm show crds " + repoName + "/" + name + " " + v;
        return executeHelm(command);
    }

    /**
     * show all
     * 一次性展示所有，value.yaml、chart.yaml、readme.md
     */
    @Override
    public HelmResultVo showAll(String name, String version) {
        if (name.isEmpty()) {
            throw new RuntimeException("name不能为空");
        }
        String command;
        // 判断有无指定版本，默认最新版本
        String v = version.isEmpty() ? "" : " --version ";
        command = "helm show all " + repoName + "/" + name + " " + v;
        return executeHelm(command);
    }

    /**
     * 打包 package
     */
    @Override
    public HelmResultVo package2Tgz(String packageName) {
        if (packageName.isEmpty()) {
            throw new RuntimeException("packageName不能为空");
        }
        // helm打包
        String command = "cd " + basePath + "/helm" + " & " + "helm package " + packageName;
        return executeHelm(command);
    }

    /**
     * 校验 helm lint
     */
    @Override
    public HelmResultVo helmLint(String packagePath) {
        if (packagePath.isEmpty()) {
            throw new RuntimeException("packagePath不能为空");
        }
        String command = "helm lint " + packagePath;
        return executeHelm(command);
    }

    /**
     * 校验 helm repo lint
     * name -chart name
     */
    @Override
    public HelmResultVo helmRepoLint(String name, String version) {
        if (name.isEmpty()) {
            throw new RuntimeException("name不能为空");
        }
        // 判断有无指定版本，默认最新版本
        String v = version.isEmpty() ? "" : " --version " + version;
        String command = "helm lint " + repoName + "/" + name + " " + v;
        return executeHelm(command);
    }

    /**
     * 渲染 helm template
     */
    @Override
    public HelmResultVo helmTemplate(String packagePath) {
        if (packagePath.isEmpty()) {
            throw new RuntimeException("packagePath不能为空");
        }
        String command = "helm template " + packagePath + " --debug";
        return executeHelm(command);
    }

    /**
     * 渲染 helm repo template
     */
    @Override
    public HelmResultVo helmRepoTemplate(String name, String version) {
        if (name.isEmpty()) {
            throw new RuntimeException("name不能为空");
        }
        // 判断有无指定版本，默认最新版本
        String v = version.isEmpty() ? "" : " --version " + version;
        String command = "helm template " + repoName + "/" + name + " " + v;
        return executeHelm(command);
    }

    /**
     * debug
     */
    @Override
    public HelmResultVo helmInstallDebug(String packageName) {
        String namespace = "default";
        String command = "helm install " + basePath + packageName + " --dry-run --debug" + " -n " + namespace;
        return executeHelm(command);
    }

    /**
     * repo debug
     * 使用仓库制品debug
     */
    @Override
    public HelmResultVo helmRepoInstallDebug(String releaseName, String name, String version) {
        String namespace = "default";
        // 判断是否指定版本，默认最新
        String v = version.isEmpty() ? "" : " --version " + version;
        String command = "helm install " + releaseName + " " + repoName + "/" + name + " " + v + " --dry-run --debug " + " -n " + namespace;
        return executeHelm(command);
    }


    /**
     * helm get manifest
     */
    @Override
    public HelmResultVo helmManifest(String releaseName, String namespace) {
        if (releaseName.isEmpty()) {
            throw new RuntimeException("releaseName不能为空");
        }
        String command = "helm get manifest " + releaseName + " -n " + namespace;
        return executeHelm(command);
    }

    /**
     * helm get all
     */
    @Override
    public HelmResultVo helmGetAll(String releaseName, String namespace) {
        if (releaseName.isEmpty()) {
            throw new RuntimeException("releaseName不能为空");
        }
        String command = "helm get all " + releaseName + " -n " + namespace;
        return executeHelm(command);
    }


    /**
     * helm安装
     */
    @Override
    public HelmResultVo helmInstall(String releaseName, String version, String[] json, String name, String namespace) {
        String command;
        String v = version.isEmpty() ? "" : " --version " + version;
        // 判断传参情况
        if (json.length == 0) {
            // 未指定参数
            command = "helm install " + releaseName + " " + repoName + "/" + name + " " + v + " -n " + namespace;
        } else {
            // 指定了参数
            String value = String.join(" --set-json=", json);
            command = "helm install " + value + " " + releaseName + " " + repoName + "/" + name + " " + v + " -n " + namespace;
        }
        return executeHelm(command);
    }

    /**
     * helm list
     */
    @Override
    public HelmResultVo helmList(String namespace, String output) {
        String command;
        // 判断是否是其他输出格式
        String o = output.isEmpty() ? "" : " --output " + output;
        // 判断获取范围
        if (namespace.isEmpty()) {
            // 未指定namespace就是获取全部的release
            command = "helm list -a -A " + o;
        } else {
            // 指定了namespace就是获取那个namespace下的release
            command = "helm list -n " + namespace + o;
        }
        return executeHelm(command);
    }

    /**
     * helm status
     */
    @Override
    public HelmResultVo helmStatus(String releaseName, String namespace) {
        if (releaseName.isEmpty()) {
            throw new RuntimeException("releaseName不能为空");
        }
        if (namespace.isEmpty()) {
            throw new RuntimeException("namespace不能为空");
        }
        // 获取指定namespace下的某个releaseName状态信息
        String command = "helm status " + releaseName + " -n " + namespace;
        return executeHelm(command);
    }

    /**
     * helm test
     * 执行发布版本的测试
     */
    @Override
    public HelmResultVo helmTest(String releaseName, String namespace) {
        if (releaseName.isEmpty()) {
            throw new RuntimeException("releaseName不能为空");
        }
        if (namespace.isEmpty()) {
            throw new RuntimeException("namespace不能为空");
        }
        // 获取指定namespace下的某个releaseName状态信息
        String command = "helm test " + releaseName + " -n " + namespace;
        return executeHelm(command);
    }

    /**
     * helm卸载
     */
    @Override
    public HelmResultVo helmUninstall(String releaseName, String namespace) {
        // 卸载指定namespace下的releaseName
        String command = "helm uninstall " + releaseName + " -n " + namespace;
        return executeHelm(command);
    }

    /**
     * 更新仓库
     */
    @Override
    public HelmResultVo helmRepoUpdate(String repoName) {
        // 判断更新范围，不指定就是更新所有
        String r = repoName.isEmpty() ? "" : repoName;
        String command = "helm repo update " + r;
        return executeHelm(command);
    }

    /**
     * 更新应用
     */
    @Override
    public HelmResultVo update(String releaseName, String[] json, String name, String namespace) {
        // 更新release
        String value = String.join(" --set-json=", json);
        String command = "helm upgrade " + value + " " + releaseName + " " + name + " -n " + namespace;
        return executeHelm(command);
    }

    /**
     * repoSearch
     * 搜索仓库
     */
    @Override
    public HelmResultVo helmRepoSearch(String name, String repoName, String version, String output) {
        if (name.isEmpty()) {
            throw new RuntimeException("name制品名称不能为空");
        }
        String command;
        // 判断是否有指定输出格式，默认table
        String o = (output.isEmpty() ? "" : " --output " + output);
        // 判断是否指定版本，默认最新版本
        String v = (output.isEmpty() ? "" : " --version " + version);
        // 仓库搜索范围
        if (repoName.isEmpty()) {
            // 指定版本查询
            command = "helm search hub " + repoName + " " + name + " " + v + " " + o;
        } else {
            // 指定了repoName仓库名就查询下面的
            command = "helm search repo " + repoName + " " + name + " " + v + " " + o;
        }
        return executeHelm(command);
    }

    /**
     * repo add
     * 添加仓库
     */
    @Override
    public HelmResultVo helmRepoAdd(String name, String url) {
        if (name.isEmpty()) {
            throw new RuntimeException("name仓库名不能为空");
        }
        if (url.isEmpty()) {
            throw new RuntimeException("url地址不能为空");
        }
        String command = "helm repo add " + name + " " + url;
        return executeHelm(command);
    }

    /**
     * repo list
     * 列出仓库
     */
    @Override
    public HelmResultVo helmRepoList(String output) {
        String o = output.isEmpty() ? "" : " --output " + output;
        // 输出格式，支持json，yaml，table，默认table
        String command = "helm repo list " + o;
        return executeHelm(command);
    }

    /**
     * repo remove
     * 移除仓库
     */
    @Override
    public HelmResultVo helmRepoRemove(String[] repoNames) {
        if (repoNames.length == 0) {
            throw new RuntimeException("repoNames仓库名不能为空");
        }
        String command = "helm repo remove " + String.join(" ", repoNames);
        return executeHelm(command);
    }

    /**
     * helm history
     * 检索发布历史
     */
    @Override
    public HelmResultVo helmHistory(String releaseName, String namespace, String output) {
        if (releaseName.isEmpty()) {
            throw new RuntimeException("releaseName不能为空");
        }
        // 判断有无指定其他格式，默认table
        String o = output.isEmpty() ? "" : " --output " + output;
        // 判断是否指定namespace
        String n = namespace.isEmpty() ? "" : " -n " + namespace;
        String command = "helm history " + releaseName + " " + n + " " + o;
        return executeHelm(command);
    }

    /**
     * helm env
     * 环境
     */
    @Override
    public HelmResultVo helmEnv() {
        String command = "helm env ";
        return executeHelm(command);
    }

    /**
     * helm rollback
     * 回滚发布到上一个版本
     */
    @Override
    public HelmResultVo helmRollBack(String releaseName, String version, boolean isForce) {
        // 是否强制回滚
        String f = isForce ? " --force " : "";
        String command = "helm rollback " + releaseName + " " + version + " " + f;
        return executeHelm(command);
    }

    /**
     * plugin
     * 获取插件
     */
    @Override
    public HelmResultVo helmPlugin() {
        String command = "helm plugin list ";
        return executeHelm(command);
    }

    /**
     * plugin update
     * 更新插件
     */
    @Override
    public HelmResultVo helmPluginUpdate(String[] pluginNames) {
        if (pluginNames.length == 0) {
            throw new RuntimeException("pluginNames不能为空");
        }
        String command = "helm plugin update " + String.join(" ", pluginNames);
        return executeHelm(command);
    }

    /**
     * plugin uninstall
     * 卸载插件
     */
    @Override
    public HelmResultVo helmPluginUninstall(String[] pluginNames) {
        if (pluginNames.length == 0) {
            throw new RuntimeException("pluginNames不能为空");
        }
        String command = "helm plugin uninstall " + String.join(" ", pluginNames);
        return executeHelm(command);
    }

    /**
     * plugin install
     * 安装插件
     */
    @Override
    public HelmResultVo helmPluginInstall(String[] urls) {
        if (urls.length == 0) {
            throw new RuntimeException("urls不能为空");
        }
        String command = "helm plugin install " + String.join(" ", urls);
        return executeHelm(command);
    }

    /**
     * 执行command
     */
    private HelmResultVo executeHelm(String command) {
        String message = "";
        List<String> data = new ArrayList<>();
        int exitCode = 1;
        Process process = null;
        try {
            //启动进程
            process = Runtime.getRuntime().exec(command);
            // 获取命令执行的输入流
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), StandardCharsets.UTF_8));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                // 记录正确结果
                data.add(line);
            }
            // 获取命令执行的错误流
            BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream(), StandardCharsets.UTF_8));
            while ((line = errorReader.readLine()) != null) {
                System.out.println(line);
                // 记录错误结果
                message = line;
            }
            // 退出码
            exitCode = process.waitFor();
            if (exitCode == 0) {
                message = "执行成功";
            }
            //关闭流释放资源
            process.getOutputStream().close();
            reader.close();
            errorReader.close();
        } catch (IOException | InterruptedException e) {
            // 记录异常信息
            message = e.getMessage();
            e.printStackTrace();
        } finally {
            if (process != null) {
                // 销毁当前进程，阻断当前命令执行
                process.destroy();
            }
        }
        String logMessage = "执行命令：" + command + ",执行结果：" + (exitCode == 0 ? "成功" : "失败");
        log.info(logMessage);
        return HelmResultVo.deal(exitCode, message, data, logMessage);
    }
}
