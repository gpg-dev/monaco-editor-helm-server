package vip.gpg123.helm;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.core.util.ZipUtil;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.zip.ZipFile;

/**
 * @author gaopuguang_zz
 * @version 1.0
 * @description: TODO
 * @date 2023/8/10 17:45
 */
public class UrlFileTest {

    private static final String FILE_URL = "http://101.89.187.179:8081/repository/helm-repo/app-0.1.0.tgz";

    /**
     * 测试解析url文件
     */
    @Test
    public void analysisUrlFile() {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            // 读取输入流
            inputStream = URLUtil.getStream(new URL(FILE_URL));
            // 读入到临时文件
            File tempFile = FileUtil.createTempFile();

            outputStream = FileUtil.getOutputStream(tempFile);
            // 拷贝到文件
            IoUtil.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
            // 拷贝完成后
            long fileSize = FileUtil.size(tempFile);
            if (fileSize == 0) {
                throw new RuntimeException("拷贝失败，文件大小不能为0");
            }
            System.out.println("拷贝成功：大小-" + fileSize + ",位置-" + tempFile.getAbsoluteFile());
            List<String> fileList = ZipUtil.listFileNames(new ZipFile(new File(tempFile.getAbsolutePath()), StandardCharsets.UTF_8), null);
            fileList.forEach(s -> {
                System.out.println(s);
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
