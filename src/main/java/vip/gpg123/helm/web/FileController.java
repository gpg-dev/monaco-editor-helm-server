package vip.gpg123.helm.web;

import cn.hutool.core.io.FileUtil;
import io.fabric8.istio.api.networking.v1alpha3.VirtualService;
import io.fabric8.istio.api.networking.v1alpha3.VirtualServiceBuilder;
import io.fabric8.kubernetes.api.model.ConfigMap;
import io.fabric8.kubernetes.api.model.ConfigMapBuilder;
import io.fabric8.kubernetes.api.model.PersistentVolume;
import io.fabric8.kubernetes.api.model.PersistentVolumeBuilder;
import io.fabric8.kubernetes.api.model.PersistentVolumeClaim;
import io.fabric8.kubernetes.api.model.PersistentVolumeClaimBuilder;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodBuilder;
import io.fabric8.kubernetes.api.model.Secret;
import io.fabric8.kubernetes.api.model.SecretBuilder;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.ServiceBuilder;
import io.fabric8.kubernetes.api.model.apps.Deployment;
import io.fabric8.kubernetes.api.model.apps.DeploymentBuilder;
import io.fabric8.kubernetes.api.model.batch.v1beta1.CronJob;
import io.fabric8.kubernetes.api.model.batch.v1beta1.CronJobBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yaml.snakeyaml.Yaml;
import vip.gpg123.helm.domain.TreeNode;
import vip.gpg123.helm.util.R;
import vip.gpg123.helm.util.YamlStyleUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author gaopuguang
 * @date 2023/7/14 8:51
 **/
@RestController
@RequestMapping("/file/")
@Slf4j
public class FileController {

    /**
     * 读目录
     */
    @GetMapping("showFiles")
    public R<List<Map<String, String>>> showFiles(@RequestParam("path") String path) {
        List<Map<String, String>> mapList = new ArrayList<>();
        try {
            File[] ls = FileUtil.ls(path);
            //fileList.stream().map(String::toString).forEach(log::info);
            //loopFiles.stream().map(File::toString).forEach(log::info);
            Arrays.stream(ls).map(File::getName).forEach(log::info);
            mapList.add(new LinkedHashMap<String, String>() {{
                put("name", "../");
                put("path", FileUtil.getParent(path, 1));
                put("absolutePath", FileUtil.getParent(path, 1));
                put("isFile", String.valueOf(false));
            }});
            Arrays.stream(ls).forEach(file -> {
                boolean isFile = FileUtil.isFile(file.getAbsoluteFile());
                mapList.add(new LinkedHashMap<String, String>() {{
                    put("name", file.getName());
                    put("AbsolutePath", file.getAbsolutePath());
                    put("path", file.getPath());
                    put("isFile", String.valueOf(isFile));
                }});
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return R.ok(mapList);
    }

    /**
     * 树结构
     */
    @GetMapping("showTree")
    public R<Object> showTree(@RequestParam("path") String path, @RequestParam("key") String key) {
        try {
            File file = new File(path);
            TreeNode node = new TreeNode();
            //node.setId(0);
            node.setTitle(file.getName());
            node.setPath(file.getPath());
            node.setKey(key);
            node.setIsLeaf(file.isFile());
            node.setChildren(tree(file.getPath(), key));
            node.setParentPath(file.getParent());
            return R.ok(node);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 读文件
     */
    @GetMapping("getFile")
    public R<String[]> getFile(@RequestParam("path") String path) {
        String[] arr = new String[0];
        if (FileUtil.exist(path)) {
            arr = file2StringArray(new File(path));
        }
        return R.ok(arr);
    }

    /**
     * 写文件
     */
    @PostMapping("/saveFile")
    public R<String> saveFile(@RequestBody Map<String, String> map) {
        try {
            String path = map.get("path");
            String data = map.get("data");
            if (FileUtil.exist(path)) {
                //创建字符写入流对象
                System.out.println("测试输出：");
                FileWriter fileWriter = new FileWriter(path);
                fileWriter.write(data);
                fileWriter.close();
                return R.ok("保存成功");
            } else {
                throw new RuntimeException("文件不存在");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 创建文件
     */
    @PostMapping("/createFile")
    public R<Boolean> createFile(@RequestBody Map<String, String> map) {
        String filePath = map.get("filePath");
        String fileName = map.get("fileName");
        String radioValue = map.get("radioValue");
        boolean success;
        Yaml yaml;
        File file = new File(filePath + "/" + fileName);
        try {
            FileWriter fw = new FileWriter(file, false);
            switch (radioValue) {
                case "1":
                    Deployment deployment = new DeploymentBuilder().withNewMetadata().endMetadata().withNewSpec().endSpec().build();
                    yaml = YamlStyleUtil.defaultYaml(Deployment.class);
                    yaml.dump(deployment, fw);
                    break;
                case "2":
                    Pod pod = new PodBuilder().withNewMetadata().endMetadata().withNewSpec().endSpec().build();
                    yaml = YamlStyleUtil.defaultYaml(Pod.class);
                    yaml.dump(pod, fw);
                    break;
                case "3":
                    Service service = new ServiceBuilder().withNewMetadata().endMetadata().withNewSpec().endSpec().build();
                    yaml = YamlStyleUtil.defaultYaml(Service.class);
                    yaml.dump(service, fw);
                    break;
                case "4":
                    PersistentVolume persistentVolume = new PersistentVolumeBuilder().withNewMetadata().endMetadata().withNewSpec().endSpec().build();
                    yaml = YamlStyleUtil.defaultYaml(PersistentVolume.class);
                    yaml.dump(persistentVolume, fw);
                    break;
                case "5":
                    PersistentVolumeClaim persistentVolumeClaim = new PersistentVolumeClaimBuilder().withNewMetadata().endMetadata().withNewSpec().endSpec().build();
                    yaml = YamlStyleUtil.defaultYaml(PersistentVolumeClaim.class);
                    yaml.dump(persistentVolumeClaim, fw);
                    break;
                case "6":
                    ConfigMap configMap = new ConfigMapBuilder().withNewMetadata().endMetadata().build();
                    yaml = YamlStyleUtil.defaultYaml(ConfigMap.class);
                    yaml.dump(configMap, fw);
                    break;
                case "7":
                    VirtualService virtualService = new VirtualServiceBuilder().withNewMetadata().endMetadata().withNewSpec().endSpec().build();
                    yaml = YamlStyleUtil.defaultYaml(VirtualService.class);
                    yaml.dump(virtualService, fw);
                    break;
                case "8":
                    CronJob cronJob = new CronJobBuilder().withNewMetadata().endMetadata().withNewSpec().endSpec().build();
                    yaml = YamlStyleUtil.defaultYaml(CronJob.class);
                    yaml.dump(cronJob, fw);
                case "9":
                    Secret secret = new SecretBuilder().withNewMetadata().endMetadata().build();
                    yaml = YamlStyleUtil.defaultYaml(Secret.class);
                    yaml.dump(secret, fw);
                case "10":
                    fw.write("");
                    break;
            }
            // 检查文件
            success = FileUtil.exist(new File(filePath));
            fw.close();
            log.info("{}/{}", filePath, fileName);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return R.ok(success).setMsg("创建成功:" + file.getPath());
    }

    /**
     * 删除
     */
    @DeleteMapping("/delete")
    public R<Boolean> deleteFile(@RequestParam("filePath") String filePath, @RequestParam("fileName") String fileName) {
        try {
            boolean isExist = FileUtil.exist(filePath + "/" + fileName);
            if (isExist) {
                boolean isDel = FileUtil.del(filePath + "/" + fileName);
                return isDel ? R.ok(true).setMsg("删除成功") : R.failed("删除失败");
            } else {
                return R.failed("文件不存在");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 校验
     */
    @GetMapping("/check")
    public String check(@RequestParam("path") String path) {

        return null;
    }

    private static String[] file2StringArray(File file) {
        BufferedReader br = null;
        List<String> list = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(file));
            String str = null;
            while ((str = br.readLine()) != null) {
                list.add(str);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list.toArray(new String[0]);
    }

    private List<TreeNode> tree(String path, String key) {
        List<TreeNode> list = new ArrayList<>();
        File[] fileList = FileUtil.ls(path);
        if (fileList.length != 0) {
            for (int i = 0; i < fileList.length; i++) {
                //
                boolean isFile = FileUtil.isFile(fileList[i].getPath());
                TreeNode node = new TreeNode();
                node.setId(0);
                node.setTitle(fileList[i].getName());
                node.setKey(key + "-" + i);
                node.setIsLeaf(isFile);
                node.setPath(fileList[i].getPath());
                node.setParentPath(fileList[i].getParent());
                list.add(node);
            }
        }
        return list;
    }


    /**
     * 获取指定字符串出现的次数
     *
     * @param srcText  源字符串
     * @param findText 要查找的字符串
     * @return
     */
    public static int appearNumber(String srcText, String findText) {
        int count = 0;
        Pattern p = Pattern.compile(findText);
        Matcher m = p.matcher(srcText);
        while (m.find()) {
            count++;
        }
        return count;
    }
}
