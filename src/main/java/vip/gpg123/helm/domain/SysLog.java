package vip.gpg123.helm.domain;

/**
 * @author gaopuguang
 * @date 2023/7/25 10:45
 **/

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 系统日志
 *
 * @author gaopuguang
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_log")
public class SysLog implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId
    private Long id;

    /**
     * 日志类型
     */
    private String logType;

    /**
     * 日志内容
     */
    private String message;

    /**
     * 创建用户id
     */
    private String createBy;

    /**
     * 创建用户名称
     */
    private String createUserName;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 请求URI
     */
    private String requestUri;

    /**
     * 请求方式
     */
    private String requestMethod;

    /**
     * 请求参数
     */
    private String requestParams;

    /**
     * 请求IP
     */
    private String requestIp;

    /**
     * 请求服务器地址
     */
    private String serverAddress;

    /**
     * 是否异常
     */
    private String isException;

    /**
     * 异常信息
     */
    private String exceptionInfo;

    /**
     * 开始时间
     */
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    private LocalDateTime endTime;

    /**
     * 执行时间
     */
    private Long executeTime;

    /**
     * 用户代理
     */
    private String userAgent;

    /**
     * 操作系统
     */
    private String deviceName;

    /**
     * 浏览器名称
     */
    private String browserName;

    /**
     * 是否删除
     */
    private Integer delFlag;
}
