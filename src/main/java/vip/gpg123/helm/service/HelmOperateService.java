package vip.gpg123.helm.service;


import vip.gpg123.helm.domain.HelmResultVo;

import java.io.IOException;

/**
 * @author gaopuguang
 * @date 2023/6/7 15:54
 **/
public interface HelmOperateService {

    /**
     * helm-client
     * 获取客户端版本
     */
    HelmResultVo getHelmVersion();

    /**
     * helm createHelmPackage
     * 创建包，default方式
     * @return HelmResultVo
     */
    HelmResultVo createHelmPackage(String name);

    /**
     * helm show values
     * 展示values.yaml文件-输出yaml格式
     * @param name chart名
     * @param version chart版本，不指定即默认最新
     * @return HelmResultVo
     */
    HelmResultVo showValues(String name, String version);

    /**
     * show values json
     * 展示values.yaml文件-输出json格式
     * @param name chart名
     * @param version chart版本，不指定即默认最新
     * @return HelmResultVo
     */
    HelmResultVo showValuesJson(String name, String version) throws IOException;

    /**
     * show readMe
     * 展示readme.md文件
     * @param name chart名
     * @param version chart版本，不指定即默认最新
     * @return HelmResultVo
     */
    HelmResultVo showReadMe(String name, String version);

    /**
     * show chart
     * 展示chart.yaml
     * @param name chart名
     * @param version chart版本，不指定即默认最新
     * @return HelmResultVo
     */
    HelmResultVo showChart(String name, String version);

    /**
     * show crd
     * 展示crd
     * @param name chart名
     * @param version chart版本，不指定即默认最新
     * @return HelmResultVo
     */
    HelmResultVo showCrd(String name, String version);

    /**
     * show all
     * 一次性展示所有，value.yaml、chart.yaml、readme.md
     * @param name chart名
     * @param version chart版本，不指定即默认最新
     * @return HelmResultVo
     */
    HelmResultVo showAll(String name, String version);

    /**
     * package
     * 打包
     * @param packageName 包名
     * @return HelmResultVo
     */
    HelmResultVo package2Tgz(String packageName);

    /**
     * helm lint
     * 本地chart包校验
     * @param packagePath 包路径
     * @return HelmResultVo
     */
    HelmResultVo helmLint(String packagePath);

    /**
     * helm repo lint
     * 仓库chart校验
     * @param name chart包名
     * @param version chart包版本，不指定即默认最新
     * @return HelmResultVo
     */
    HelmResultVo helmRepoLint(String name, String version);

    /**
     * helm template
     * 本地chart渲染
     * @param packagePath 包路径
     * @return HelmResultVo
     */
    HelmResultVo helmTemplate(String packagePath);

    /**
     *  helm repo template
     *  仓库chart渲染
     * @param name chart包名
     * @param version chart包版本，不指定即默认最新
     * @return HelmResultVo
     */
    HelmResultVo helmRepoTemplate(String name, String version);

    /**
     * debug
     * 本地chart-debug
     * @param packageName chart包名
     * @return HelmResultVo
     */
    HelmResultVo helmInstallDebug(String packageName);

    /**
     * repo debug
     * 仓库chart-debug
     * @param releaseName 发布名称
     * @param name chart名称
     * @param version chart版本，不指定即默认最新
     * @return HelmResultVo
     */
    HelmResultVo helmRepoInstallDebug(String releaseName,String name,String version);

    /**
     * helm get manifest
     * 获取发布清单
     * @param releaseName 发布名
     * @param namespace 命名空间
     * @return HelmResultVo
     */
    HelmResultVo helmManifest(String releaseName, String namespace);

    /**
     * helm install
     * 安装chart
     * @param releaseName 发布名
     * @param version chart版本号，默认最新
     * @param json 参数
     * @param name chart名
     * @param namespace 命名空间
     * @return HelmResultVo
     */
    HelmResultVo helmInstall(String releaseName, String version, String[] json, String name, String namespace);

    /**
     * helm list
     * 列出helm应用
     * @param namespace 命名空间，不指定即所有
     * @param output 输出格式，支持json、table、yaml 不指定即默认table
     * @return HelmResultVo
     */
    HelmResultVo helmList(String namespace, String output);

    /**
     * helm status
     * 获取某个应用状态
     * @param releaseName 发布名称
     * @param namespace 命名空间
     * @return HelmResultVo
     */
    HelmResultVo helmStatus(String releaseName, String namespace);

    /**
     * helm test
     * 执行发布版本的测试
     * @param releaseName 发布名称
     * @param namespace 命名空间
     * @return HelmResultVo
     */
    HelmResultVo helmTest(String releaseName, String namespace);

    /**
     * helm get all
     * 获取所有应用信息
     * @param releaseName 发布名称
     * @param namespace 命名空间
     * @return HelmResultVo
     */
    HelmResultVo helmGetAll(String releaseName, String namespace);

    /**
     * helm uninstall
     * 卸载
     * @param releaseName 发布名称
     * @param namespace 命名空间
     * @return HelmResultVo
     */
    HelmResultVo helmUninstall(String releaseName, String namespace);

    /**
     * helm repo update
     * 更新仓库
     * @param repoName 仓库名称
     * @return HelmResultVo
     */
    HelmResultVo helmRepoUpdate(String repoName);

    /**
     * helm upgrade
     * 更新应用
     * @param releaseName 发布名称
     * @param name chart名称
     * @param json 参数
     * @param namespace 命名空间
     * @return HelmResultVo
     */
    HelmResultVo update(String releaseName, String[] json, String name, String namespace);

    /**
     * helm repo search
     * 搜索仓库
     * @param name  chart名
     * @param repoName 仓库名
     * @param version chart版本
     * @param output 输出格式，支持json、table、yaml 不指定即默认table
     * @return HelmResultVo
     */
    HelmResultVo helmRepoSearch(String name, String repoName, String version, String output);

    /**
     * repo add
     * 添加仓库
     * @param name 仓库名
     * @param url 仓库地址
     * @return HelmResultVo
     */
    HelmResultVo helmRepoAdd(String name, String url);

    /**
     * helm repo list
     * 列出已添加的仓库
     * @param output 输出格式，支持json、table、yaml 不指定即默认table
     * @return HelmResultVo
     * @return HelmResultVo
     */
    HelmResultVo helmRepoList(String output);

    /**
     * repoRemove
     * 移除仓库
     * @param repoNames 仓库名
     * @return HelmResultVo
     */
    HelmResultVo helmRepoRemove(String[] repoNames);

    /**
     * history
     * 检索发布历史
     * @param releaseName 发布名称
     * @param namespace 命名空间
     * @param output 输出格式，支持json、table、yaml 不指定即默认table
     * @return HelmResultVo
     */
    HelmResultVo helmHistory(String releaseName, String namespace, String output);

    /**
     * env
     * 获取环境变量
     * @return HelmResultVo
     */
    HelmResultVo helmEnv();

    /**
     * rollback
     * 回滚发布到上一个版本
     * @param releaseName 发布名称
     * @param version 版本号
     * @param isForce 强制回滚
     * @return HelmResultVo
     */
    HelmResultVo helmRollBack(String releaseName, String version, boolean isForce);

    /**
     * plugin
     * 获取插件
     * @return HelmResultVo
     */
    HelmResultVo helmPlugin();

    /**
     * plugin update
     * 更新插件
     * @param pluginNames 插件名
     * @return HelmResultVo
     */
    HelmResultVo helmPluginUpdate(String[] pluginNames);

    /**
     * plugin uninstall
     * 卸载插件
     * @param pluginNames 插件名
     * @return HelmResultVo
     */
    HelmResultVo helmPluginUninstall(String[] pluginNames);

    /**
     * plugin install
     * 安装插件
     * @param  urls 插件地址
     * @return HelmResultVo
     */
    HelmResultVo helmPluginInstall(String[] urls);


}
