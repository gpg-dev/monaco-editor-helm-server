create table cscp_helm_component
(
    id               bigint auto_increment
        primary key,
    component_name   varchar(64)                          null comment '组件名称',
    component_icon   text                                 null comment '组件图标',
    component_desc   varchar(200)                         null comment '组件描述',
    component_tags   text                                 null comment '标签',
    release_name     varchar(50)                          null comment '发布类型',
    version          varchar(50)                          null comment '版本',
    type             tinyint(1) default 1                 null comment '类型，0-系统，1-个人上传',
    create_by        varchar(64)                          null,
    update_by        varchar(64)                          null,
    create_user_name varchar(64)                          null comment '创建者用户名',
    update_user_name varchar(64)                          null comment '更新者用户名',
    create_time      datetime   default CURRENT_TIMESTAMP null,
    update_time      datetime   default CURRENT_TIMESTAMP null,
    del_flag         tinyint(1) default 0                 null,
    constraint id
        unique (id)
)
    comment '组件制品';

