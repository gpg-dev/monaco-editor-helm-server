package vip.gpg123.helm;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.json.JSONUtil;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author gaopuguang_zz
 * @version 1.0
 * @description: TODO
 * @date 2023/8/30 15:33
 */
public class BuildTest {

    /**
     * 构建tree
     */
    @Test
    public void testBuild() {
        // 构建node列表(数据源)
        List<TreeNode<String>> nodeList = new ArrayList<>();
        nodeList.add(new TreeNode<>("1", "0", "系统管理", 5));
        nodeList.add(new TreeNode<>("11", "1", "用户管理", 222222));
        nodeList.add(new TreeNode<>("111", "11", "用户添加", 0));
        nodeList.add(new TreeNode<>("2", "0", "店铺管理", 1));
        nodeList.add(new TreeNode<>("21", "2", "商品管理", 44));
        nodeList.add(new TreeNode<>("221", "2", "商品管理2", 2));
        // 0表示最顶层的id是0
        List<Tree<String>> treeList = TreeUtil.build(nodeList, "0");
        String jsonStr = JSONUtil.toJsonStr(treeList);
        String jsonStrFmt = JSONUtil.formatJsonStr(jsonStr);
        System.out.println(jsonStrFmt);
    }

    /**
     * 获取loop目录
     */
    @Test
    public void listContent() throws IOException {
        List<TreeNode<String>> nodeList = new ArrayList<>();
        String contentPath = "D:\\project\\server\\helm-repo\\my-test";
        List<File> fileList = FileUtil.loopFiles(contentPath);
        System.out.println(fileList.size());
        char chart = '\\';
        for (int i = 0; i < fileList.size(); i++) {
            // 判断文件深度
            String filePath = fileList.get(i).getPath();
            int contentCount = countChar(contentPath, chart);
            int fileCount = countChar(filePath, chart);
            // 扩展字段
            File file = fileList.get(i);
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("path", file.getPath());
            map.put("absolutePath", file.getAbsolutePath());
            map.put("parent",file.getParent());
            map.put("canWrite", file.canWrite());
            map.put("canRead", file.canRead());
            map.put("isFile", file.isFile());
            map.put("lastModified", DateUtil.date(file.lastModified()).toString());
            map.put("isHidden", file.isHidden());
            map.put("usableSpace", file.getUsableSpace());
            map.put("freeSpace", file.getFreeSpace());
            map.put("totalSpace",file.getTotalSpace());

            // 构造tree
            TreeNode<String> node = new TreeNode<>();
            node.setId(String.valueOf(i));
            node.setParentId(String.valueOf(fileCount - contentCount - 1));
            node.setName(fileList.get(i).getName());
            node.setWeight(file.isFile() ? 0 : 1);
            node.setExtra(map);
            nodeList.add(node);

            System.out.println(fileList.get(i).getPath());
        }
        // 0表示最顶层的id是0
        List<Tree<String>> treeList = TreeUtil.build(nodeList, "0");
        String jsonStr = JSONUtil.toJsonStr(treeList);
        String jsonStrFmt = JSONUtil.formatJsonStr(jsonStr);
        System.out.println(jsonStrFmt);
        File file = new File("D:/example.json");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(jsonStrFmt);
        fileWriter.close();
    }

    /**
     * ls
     */
    @Test
    public void listFileList() {
        String contentPath = "D:\\project\\server\\helm-repo\\my-test";
    }

    @Test
    public void test() {
        String a = "aabbccc";
        char chart = 'a';
        int count = countChar(a, chart);
    }

    /**
     *
     */
    public static int countChar(String str, char c) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c) {
                count++;
            }
        }
        return count;
    }
}
