package vip.gpg123.helm.web;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import vip.gpg123.helm.domain.HelmResultVo;
import vip.gpg123.helm.service.ChartService;
import vip.gpg123.helm.service.HelmOperateService;
import vip.gpg123.helm.util.R;

import java.io.IOException;

/**
 * @author gaopuguang_zz
 * @version 1.0
 * @description: TODO
 * @date 2023/8/14 14:03
 */
@RequestMapping("/chart/")
@RestController
@Slf4j
public class ChartController {

    @Autowired
    private HelmOperateService helmOperateService;

    @Autowired
    private ChartService chartService;

    /**
     * 上传chart
     *
     * @param file chart文件
     */
    @PostMapping("upload")
    @ApiOperation(value = "upload", notes = "上传chart文件")
    public R<Boolean> upload(@RequestParam("file") MultipartFile file) throws IOException {
        return chartService.uploadChart(file);
    }

    /**
     * 获取所有
     */
    @GetMapping("all")
    @ApiOperation(value = "all", notes = "获取所有的chart")
    public R<Object> getAll() throws IOException {
        return chartService.searchAllChart();
    }

    /**
     * 根据id查询
     */
    @GetMapping("getById")
    @ApiOperation(value = "getById", notes = "根据id查询chart")
    public R<Object> getById(@RequestParam(value = "id") String id) throws IOException {
        return chartService.searchChartById(id);
    }

    /**
     * 根据名称,版本查询-返回一个或多个
     */
    @GetMapping("/searchByName")
    @ApiOperation(value = "searchByName", notes = "根据名称,版本查询-返回一个或多个")
    public R<Object> searchByName(@RequestParam(value = "name") String name, @RequestParam(value = "version", required = false) String version) throws IOException {
        return chartService.searchChartByName(name, version);
    }

    /**
     * 根据名称获取id
     */
    @GetMapping("/getIdByName")
    @ApiOperation(value = "getIdByName", notes = "根据名称获取id")
    public R<String> getIdByName(@RequestParam("name") String name, @RequestParam("version") String version) throws IOException {
        return chartService.getChartIdByName(name, version);
    }

    /**
     * 删除chart
     */
    @DeleteMapping("delete")
    @ApiOperation(value = "delete", notes = "删除chart")
    public R<Boolean> deleteById(@RequestParam(value = "id") String id) throws IOException {
        return chartService.deleteChartById(id);
    }

    /**
     * 获取chart的Values
     */
    @GetMapping("getValues")
    @ApiOperation(value = "getValues", notes = "获取chart的Values")
    public R<Object> getValues(@RequestParam(value = "name") String name, @RequestParam(value = "version", required = false) String version) {
        HelmResultVo vo = helmOperateService.showValues(name, version);
        return R.ok(vo);
    }

    /**
     * 获取chart的Chart
     */
    @GetMapping("getChart")
    @ApiOperation(value = "getChart", notes = "获取chart的Chart")
    public R<Object> getChart(@RequestParam(value = "name") String name, @RequestParam(value = "version", required = false) String version) {
        HelmResultVo vo = helmOperateService.showChart(name, version);
        return R.ok(vo);
    }

    /**
     * 获取chart的readMe
     */
    @GetMapping("getReadMe")
    @ApiOperation(value = "getReadMe", notes = "获取chart的readme")
    public R<Object> getReadMe(@RequestParam(value = "name") String name, @RequestParam(value = "version", required = false) String version) {
        HelmResultVo vo = helmOperateService.showReadMe(name, version);
        return R.ok(vo);
    }

    /**
     * 渲染template
     */
    @GetMapping("template")
    @ApiOperation(value = "template", notes = "渲染template")
    public R<Object> getTemplate(@RequestParam(value = "name") String name, @RequestParam(value = "version", required = false) String version) {
        HelmResultVo vo = helmOperateService.helmRepoTemplate(name, version);
        return R.ok(vo);
    }


}
