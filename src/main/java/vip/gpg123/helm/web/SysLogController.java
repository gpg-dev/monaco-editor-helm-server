package vip.gpg123.helm.web;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.catalina.security.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.gpg123.helm.domain.SysLog;
import vip.gpg123.helm.service.SysLogService;
import vip.gpg123.helm.util.R;

import java.security.Security;
import java.util.List;

/**
 * @author gaopuguang
 * @date 2023/7/25 11:20
 **/
@RestController
@RequestMapping("/log/")
public class SysLogController {

    @Autowired
    private SysLogService sysLogService;

    /**
     * 分页查询日志
     */
    @GetMapping("page")
    public R<IPage<SysLog>> page(IPage<SysLog> page) {
        // 查询日志
        return R.ok(sysLogService.page(page, new LambdaQueryWrapper<SysLog>().eq(SysLog::getCreateBy, "").eq(SysLog::getDelFlag, 0)));
    }

    /**
     * 查询list
     */
    @GetMapping("all")
    public R<List<SysLog>> listAll() {
        return R.ok(sysLogService.list(new LambdaQueryWrapper<SysLog>().eq(SysLog::getCreateBy, "").eq(SysLog::getDelFlag, 0)));
    }
}
